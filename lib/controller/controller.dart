import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:weather_app_practice/model/weather_model.dart';

class Controller {
  Future<WeatherModel> fetchWeather(
      {required double lat, required double lon}) async {
    Response response = await http.get(Uri.parse(
        'https://api.openweathermap.org/data/2.5/weather?lat=$lat&lon=$lon&appid=810b9c67d42abcf04f283810b5b72bb6&units=metric'));

    var body = jsonDecode(response.body);

    WeatherModel weatherModel = WeatherModel();

    weatherModel = WeatherModel.fromJson(body);

    return weatherModel;
  }
}
