import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:weather_app_practice/controller/controller.dart';
import 'package:weather_app_practice/model/weather_model.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  WeatherModel? _weather;
  bool _isLoading = true;

  getLanLon() async {
    try {
      bool serviceEnabled;
      LocationPermission permission;

      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        return Future.error('Location service is disabled');
      }

      permission = await Geolocator.checkPermission();

      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return Future.error('Location permission is denied');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        return Future.error('Location permission is denied forever');
      }

      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      getWeather(lat: position.latitude, lon: position.longitude);
    } catch (e) {
      print(e.toString());
    }
  }

  getWeather({required double lat, required double lon}) async {
    WeatherModel weatherModel =
        await Controller().fetchWeather(lat: lat, lon: lon);
    setState(() {
      _weather = weatherModel;
      _isLoading = false;
    });
  }

  @override
  void initState() {
    getLanLon();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MY WEATHER'.toUpperCase()),
        centerTitle: true,
      ),
      body: _isLoading == false
          ? Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Text(
                    'Country: ${_weather?.sys?.country}',
                    style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    'Temp: ${_weather?.main?.temp}°',
                    style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    '${_weather?.weather?[0].main}',
                    style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
                  ),
                  Image.network(
                    'http://openweathermap.org/img/wn/${_weather?.weather?[0].icon}@4x.png',
                  ),
                ],
              ),
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}
